#!/usr/bin/perl -w

# Normalise les textes pour le traitement dans la chaîne Presto.
#
# Paramètres:
#  -d | --debug : mode debug
#  -e <Encoding> | --encoding <Encoding> : codage des fichiers (UTF-8 par défaut)
#  -i <ID> | --id <ID> : identifiant du texte dans le fichier de métadonnées
#  -v | --verbose : sortie bavarde
#  --type <Type> : ne traiter qu'un type de fichier
#
# Entrée: le *contenu* d'un texte (par exemple passé par un pipe)
# Sortie: le texte normalisé.

use strict;

use Switch;
use Getopt::Long;
use XML::Entities;
use POSIX;
use File::Basename;
use Data::Dumper;  # Utilisé pour le débug


############################
# Traitement des arguments #
############################

# Lecture des arguments
my $config_verbose = 0;
my $config_debug = 0;
my $config_fileEncoding = 'utf8';
my $config_fileName='';
my $config_id='';
my $config_type='';
GetOptions(
  'verbose|v'=>\$config_verbose,
  'filename=s'=>\$config_fileName,
  'id=s'=>\$config_id,
  'debug|d'=>\$config_debug,  # Mode débug: plante si échec de la normalisation
  'encoding=s'=>\$config_fileEncoding,
  'type=s'=>\$config_type
);

##############################
# Détection du type de texte #
##############################

my $fileName = basename($config_fileName);  # Nom fichier
my $buffer = '';
my $nbLines = 0;  # Nb de lignes dans le buffer

my $type = 'UNKNOWN';  # Type par défaut

# On essaye de lire les 100 premières lignes
while(($nbLines < 100) && (my $line = <STDIN>)) {
  $buffer .= $line;
  ++$nbLines;
}

# Identification en fonction du nom de fichier et du contenu des 100 premières lignes
# Extension .xml
if($fileName=~m/\.xml$/) {
  if($buffer=~m/<idno type="(FRANTEXT|Presto)">/i) {  # Frantext
    $type = 'FRANTEXT';
  }
  elsif($buffer=~m/copyright="Bibliothèques Virtuelles Humanistes/i) {  # BVH modifié pour TXM
    $type = 'BVH-mod';
  }
  elsif($buffer=~m;<(!DOCTYPE TEI SYSTEM |\?xml-model href=)"http://(www.bvh.univ-tours.fr|10.105.50.55).+<teiHeader>;is) {  # BVH
    $type = 'BVH';
  }
  elsif($buffer=~m/<distributor>(ACI Astrée|Projet Cyrus)/i) {  # CEPM (l'Astrée et Cyrus)
    $type = 'CEPM';
  }
  elsif($buffer=~m/<p> l'Est Républicain <\/p>/i && $buffer=~m/<name>B. Gaiffe<\/name>/i) {  # Est Républicain
    $type = 'EstRep';
  }
  elsif($buffer=~m@<article [^>]+url="/encyclopedie/[^>]++>@) { # Universalis
    $type = "Universalis";
  }
} elsif($fileName=~m/\.tei$/) {
  $type = 'ARTFL-TEI';
} elsif($fileName=~m/\.html$/) {
  if($fileName!~m/^(\.|~)/ && $buffer=~m;© Université de Poitiers.+Transcription et version html : Pierre Martin;is) {  # BVH téléchargé (HTML)
    $type = 'BVH-HTML';
  }    
  elsif($buffer=~m!- Wikipédia</title>!) {
    $type = 'Wikipedia';
  }
} elsif($fileName=~m/-sgml\.txt$/) {
  if($buffer=~m/<text id="/i && $buffer!~m/<issue year="/i) {  # SGML 20ème siècle, sauf presse
    $type = 'SGML20';
  }
  elsif($buffer=~m/<text id="/i && $buffer=~m/<issue year="/i) {  # SGML 20ème siècle, seulement presse
    $type = 'SGML20-press';
  }
  elsif($buffer=~m/<text sig="[^"]+" lemma="/im) {
    $type = 'Encyclo';
  }
} elsif($fileName=~m/\.csv$/) {
  if($fileName!~m/^(\.|~)/ && $fileName=~m/\.csv$/ && $buffer=~m/Mot n°;Forme rencontrée;Variante de lemme;Lemme Validé;CG Validée/i) {
    $type = 'Analog';                                                    
  }
  elsif($fileName!~m/^(\.|~)/ && $fileName=~m/\.csv$/ && $buffer=~m/^[^\t]+\t[^\t]+\t[^\t]+$/im) {
    $type = 'TTG';
  }
} elsif($fileName=~m/\.txt$/) {
  if($fileName=~m/volume[0-9]+\.txt$/ && $buffer=~m/-+ ARTICLE -+/i) {  # ARTFL (Encyclopédie)
    $type = 'ARTFL';
  }
  elsif($buffer=~m/<text id="[^"]+"[^>]*dateOriginalEdition=/im) {
    $type = 'Presto';
  }
  else {  # Texte brut
    $type = 'Text';
  }
} elsif($fileName=~m/\.cha$/) {
  if($buffer=~m/\@Begin/) {
    $type = 'CHAT';
  }
}

# Ne pas traiter ce type de fichier
if($config_type && $config_type ne $type) {
  print STDERR "[info] Option --type: ne pas traiter les fichiers de type $type.\n";
  exit 1;
}

# Log
if($config_verbose) {
  print STDERR "[info] Texte de type $type\n";
}

# On ne traite pas les fichiers de type inconnu
if($type eq 'UNKNOWN') {
  die("[warning] Impossible d'identifier le fichier $config_fileName; ce fichier ne sera pas traité !\n");
}


##########################
# Normalisation du texte #
##########################

# Charger le reste du fichier. (Attention, on charge tout le fichier ! Travailler sur des fichiers de taille raisonnable.)
my $fileContent = $buffer;  # Texte en cours de normalisation
undef $buffer;  # On n'utilisera plus le buffer; libérer la mémoire.
while(my $line = <STDIN>) {
  $fileContent .= $line;
  ++$nbLines;
}

sub specialCharacters {
  my ($text) = @_;
  $text=~s/\0/ /g;  # Remplacer les caractères null par des espaces (normalement il n'y en a pas...)
  $text=~s/\t/ /g;  # Remplacer les tabulations par des espaces (normalement il n'y en a pas...)
  $text=~s/\x1c/ /g;  # Remplacer un caractère de contrôle ASCII présent dans les textes de l'ARTFL
  $text=~s/&#x2019;/'/gs;  # Remplacer les apostrophies spéciales U+2019 encodées pour HTML
  return $text;
}

sub preAnnotate {
  my ($text) = @_;
  $text=~s!\*([^*\s.,:;\!?<>\(\)'"]+)!<presto:pretag pos="Np">$1</presto:pretag>!gm;
  $text=~s!(<presto:pretag pos="(.*?)">.*?)</presto:pretag><presto:pretag pos="\2">!$1!gm;
  $text=~s!<((?:fore|geog|pers|place|sur)?[nN]ame)(?: [^>]*?)?>(.+?)</\1>!<presto:pretag pos="Np">$2</presto:pretag>!gs;  # NP -> format Frantext
  $text=~s!(<foreign(?: [^>]*)?>(.*?)</foreign>)!<presto:pretag pos="Xe">$1</presto:pretag>!gs;
  return $text;
}

sub cleanTEI {
  my ($text) = @_;
  # Repérage des sections "body", "p", "div", "head" − le document est déjà en TEI, c'est facile − traitement des div0, div1...
  $text=~s!(<(body|p|div|head)[0-9]*(?: |>))!<presto:$2>$1!gm;
  $text=~s!(</(body|p|div|head)[0-9]*>)!$1</presto:$2>!gm;
  $text=~s!(<pb [^n]*+n="[^"&]*+)(?:&#[^;"]++;[^"]*)+("[^/]*+/>)!$1$2!gm;
  $text=~s!\s*<space [^/]+/>\s*! !gm;
  $text=~s!(<[^>\n]+?)\n!$1 !gm; # Certaines balises se retrouvent éclatées sur deux lignes, les rassembler
  return $text;
}

sub formatOutput {
  my ($text) = @_;
  $text=~s/(<(?!(?:lb rend="hyphen"|\/?hi))[^>]++>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
  $text=~s/^\s*//gm;  # Enlever les espaces en début de ligne
  $text=~s/\n+/\n/gs;  # Enlever les lignes vides
  return $text;
}

switch($type) {
    
  case 'FRANTEXT' {
    # Extraction des métadonnées
    my $origin = ''; $fileContent=~m!<edition>\s*(.+?)\s*</edition>.+?<publisher>(.+?)</publisher>!s and $origin = "$2 - $1";
    my $licence = ''; $fileContent=~m!<availability.*?>\s*(.+?)\s*</availability>!s and $licence = $1 and $licence=~s/<.*?>//g and $licence=~s/\s+/ /g;

    $fileContent = specialCharacters($fileContent);
    $fileContent=~s/\s+/ /g;  # Enlever les retours chariot
    $fileContent=~s!<hi rend="[^"]++">((?:[^<]|<[^/]+/>|\R)+)</hi>\n?!$1!gs;

    $fileContent = preAnnotate($fileContent);
    $fileContent = cleanTEI($fileContent);
    $fileContent = formatOutput($fileContent);
  }

  case 'BVH-mod' {  # BVH modifié pour TXM
    my $origin = ''; $fileContent=~m!edsci="(.+?)" edcomm="(.+?)"!s and $origin = "$2 - $1"; 
    my $licence = ''; $fileContent=~m!statut="(.+?)"!s and $licence = $1 and $licence=~s/\s+/ /g;

    $fileContent = specialCharacters($fileContent);
    $fileContent=~s/\s+/ /g;  # Enlever les retours chariot
    $fileContent=~s!<w [^>]+>(.*?)</w>!$1!gs;  # Enlever les balises <w>

    # Échapper les notes
    $fileContent=~s!(<note.+?</note>)!<presto:out>$1</presto:out>!gs;  

    # Cas particulier: les hyphens à l'intérieur d'un mot ne doivent pas être sur une nouvelle ligne
    $fileContent=~s!-?\s*(<lb rend="(:?no)?hyphen[^>]+>)\n(</lb>)\n!$1$2!gs;
    $fileContent=~s!-?\s*(<lb rend="(:?no)?hyphen[^>]+/>)\s*!$1!gs;

    $fileContent = preAnnotate($fileContent);
    $fileContent = cleanTEI($fileContent);
    $fileContent = formatOutput($fileContent);
  }

  case 'BVH' {
    my $licence = ''; $fileContent=~m!<availability.*?>\s*(.+?)\s*</availability>!s and $licence = $1 and $licence=~s/<.*?>//g and $licence=~s/\s+/ /g;

    $fileContent = specialCharacters($fileContent);

    $fileContent=~s/\s+/ /g;  # Enlever les retours chariot
    $fileContent=~s!<w [^>]+>(.*?)</w>!$1!gs;  # Enlever les balises <w>

    # Sortir le contenu des balise <hi> (highlight)
    $fileContent=~s!<hi rend="[^"]++">((?:[^<]|<[^/]+/>|\R)+)</hi>\n?!$1!gs;

    # Cas particulier: les hyphens à l'intérieur d'un mot ne doivent pas être sur une nouvelle ligne
    $fileContent=~s!-?\s*(<lb rend="(:?no)?hyphen[^>]+>)\n(</lb>)\n!$1$2!gs;
    $fileContent=~s@-?\s*(?:(?:<pb(?:[^/]|/(?!>))*?/>|<fw[^>]++>[^<]++</fw>)\s*)*(<lb rend="(:?no)?hyphen[^>]+/>)\s*@$1@gs; # Retirer les espaces et les balises <pb> et <fw> (changement de page) avant les <lb> (retour à la ligne)

    # Échapper les notes, les n° de page, les en-têtes de page, corrections
    $fileContent=~s!(<(note|fw|orig|sic)(?: [^>/]+)?>.+?</\2>)!<presto:out>$1</presto:out>!gs;
    $fileContent=~s!(<(note|fw|orig|sic)(?: [^>]+)?/>)!<presto:out>$1</presto:out>!gs;

    # Titres
    $fileContent=~s!(<title(?: |>))!<presto:head>$1!gm;
    $fileContent=~s!(</(title>))!$1</presto:head>!gm;

    $fileContent = preAnnotate($fileContent);
    $fileContent = cleanTEI($fileContent);
    $fileContent = formatOutput($fileContent);
  }

  case 'CEPM' {
    my $licence = "Licence Creative Commons : diffusion et reproduction libres avec l'obligation de citer la source et l'interdiction de toute modification et de toute utilisation commerciale";

    $fileContent = specialCharacters($fileContent);
    $fileContent=~s/&?nbsp;/ /g;

    # Take highlighted elements out into regular text
    $fileContent=~s!<hi rend="[^"]++">((?:[^<]|<[^/]+/>|\R)+)</hi>\n?!$1!gs;

    # Cas particulier: les hyphens à l'intérieur d'un mot ne doivent pas être sur une nouvelle ligne
    $fileContent=~s!\n(<lb rend="hyphen[^>]+>)\n(</lb>)\n!$1$2!gs;
    $fileContent=~s!\n(<lb rend="hyphen[^>]+/>)\n!$1!gs;

    $fileContent = cleanTEI($fileContent);
    $fileContent = formatOutput($fileContent);
  }

  case 'ARTFL' {  

    $fileContent = specialCharacters($fileContent);
    $fileContent=~s/Title Page//;

    $fileContent=~s!-+ ARTICLE -+\s*Lemma=(.*?)\s*Author=(.*?)\s*Normalized Classification=(.*?)\s*Part of Speech=(.*?)\n!</presto:div><presto:div lemma="$1" author="$2" pos="$3">!gs;  # Mettre chaque article dans un div
    $fileContent=~s!^.*?</presto:div>!<presto:body>!s;  # Premier div
    $fileContent.='</presto:div></presto:body>';  # Dernier div

    $fileContent=~s/\n/ /gs;  # Enlever les retours à la ligne
    $fileContent = formatOutput($fileContent);
  }

  case 'ARTFL-TEI' {
    $fileContent = specialCharacters($fileContent);
    $fileContent=~s/\s+/ /g;  # Enlever les retours chariot
    $fileContent=~s!<hi rend="[^"]++">((?:[^<]|<[^/]+/>|\R)+)</hi>\n?!$1!gs;

    $fileContent = preAnnotate($fileContent);
    $fileContent = cleanTEI($fileContent);
    $fileContent = formatOutput($fileContent);
  }

  case 'Text' {
    $fileContent = specialCharacters($fileContent);
      
    $fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises restantes (p et div et foreign)
    
    $fileContent = "<presto:body>\n<presto:div>\n$fileContent\n</presto:div>\n</presto:body>\n";  # Ajout de balises div
  }
  
  case 'Universalis' {
    $fileContent = specialCharacters($fileContent);
    $fileContent =~ s/\s+/ /g;  # Enlever les retours chariot
    $fileContent =~ s!(<prospects_niv1>.+?</prospects_niv1>)!<presto:out>$1</presto:out>!gs;
    $fileContent = XML::Entities::decode('all', $fileContent); # Content seems to be overencoded once
    $fileContent =~ s!(<corps(?: |>))!<presto:body>$1!gm;
    $fileContent =~ s!</corps>!$&</presto:body>!gm;
    $fileContent = formatOutput($fileContent);
  }

  case 'Wikipedia' {
    $fileContent = specialCharacters($fileContent);
    $fileContent =~ s/\s+/ /g;  # Enlever les retours chariot
    $fileContent =~ s!<div id="contentSub"></div>!$&<presto:body>!gs;
    $fileContent =~ s!<div class="printFooter">!</presto:body>$&!gs;
    $fileContent =~ s@<!--(?:[^-]|-(?!->))*-->@@gm;
    $fileContent =~ s!<table id="toc"[^>]*>.*?</table>!<presto:out>$&</presto:out>!gm;
    $fileContent =~ s!<script[^>]*>.*?</script>!<presto:out>$&</presto:out>!gm;
    $fileContent = formatOutput($fileContent);
  }

  else {
    # Type inconnu: ne pas traiter
    print STDERR "[error] Je ne sais pas traiter les fichiers de type '$type' !\n";
    $fileContent = '';  
  }

}

##########
# Sortie #
##########

# Afficher la sortie
print "<presto:text>\n";
print $fileContent;
print "</presto:text>\n";
